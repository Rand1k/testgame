package info.fandroid.game;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;

public class ExitMenuScreen implements Screen {
    final Drop game1;
    OrthographicCamera camera;


    public ExitMenuScreen(final Drop gam) {
        game1 = gam;

        camera = new OrthographicCamera();
        camera.setToOrtho(false, 800, 480);
    }
    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();

        game1.batch.setProjectionMatrix(camera.combined);
        game1.batch.begin();
        game1.font.draw(game1.batch, "You score :"+GameScreen.dropsGatchered, 240, 400);
        game1.font.draw(game1.batch, "Try again?", 240, 350);
        game1.batch.end();

        if (Gdx.input.isTouched()){
            GameScreen.dropsGatchered = 0;
            game1.setScreen(new GameScreen(game1));
            dispose();
        }
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
